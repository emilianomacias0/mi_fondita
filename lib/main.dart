import 'package:flutter/material.dart';
import 'package:mi_fondita/src/pages/alacarta_page.dart';
import 'package:mi_fondita/src/pages/inicio_page.dart';
import 'package:mi_fondita/src/pages/recetario_page.dart';
import 'package:mi_fondita/src/pages/registro_categorias_page.dart';
import 'package:mi_fondita/src/pages/settings_page.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';
import 'package:mi_fondita/src/utils/hex_colors_util.dart';
 
void main() async {
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  runApp(MyApp());
  }
 
class MyApp extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mi Fondita',
      initialRoute: 'inicio',//prefs.ultimaPagina,
      routes: {
        InicioPage.routeName:(BuildContext context) => InicioPage(),
        SettingsPage.routeName:(BuildContext context) => SettingsPage(),
        RecetarioPage.routeName:(BuildContext context) => RecetarioPage(),
        AlaCartaPage.routeName:(BuildContext context) => AlaCartaPage(),
        RegistrCategorias.routeName:(BuildContext context) => RegistrCategorias()
      },
      theme: ThemeData(
        primaryColor: HexColor('e8500e')
      ),
    );
  }
}