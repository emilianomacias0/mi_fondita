import 'package:flutter/material.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';
import 'package:mi_fondita/src/widgets/app_bar_widget.dart';
import 'package:mi_fondita/src/widgets/menu_widget.dart';

class SettingsPage extends StatefulWidget {
  static final String routeName = 'configuracion';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  TextEditingController _textController;
  bool _botonSecundario = false;
  int _genero = 1;
  int _horario = 1;
  String _nombre = 'Emiliano';
  String _ubicacion = 'Tres rios';
  List<String> _ubicaciones = ['Tres rios', 'Parque grande', 'Prologis'];
  final prefs = new PreferenciasUsuario();

  @override
  void initState() {
    prefs.ultimaPagina = SettingsPage.routeName;
    super.initState();
    _genero = prefs.genero;
    _horario = prefs.horario;
    _ubicacion = prefs.ubicacion;
    _botonSecundario = prefs.colorSecundario;
    _textController = new TextEditingController(text: prefs.nombre);
  }

  _setSelectedRadio(int valor) {
    //await prefs.setInt('genero', valor);
    prefs.genero = valor;
    setState(() {
      _genero = valor;
    });
  }

  _setSelectedHorario(int valor) {
    prefs.horario = valor;
    setState(() {
      _horario = valor;
    });
  }

_setSelectedUbicacion(String valor) {
    prefs.ubicacion = valor;
    setState(() {
      _ubicacion = valor;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _Appbar(context, 'Preferencias', prefs),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Configuración',
              style: TextStyle(fontSize: 45.0, fontWeight: FontWeight.bold),
            ),
          ),
          Divider(),
          SwitchListTile(
            value: _botonSecundario,
            title: Text('Color secundario'),
            onChanged: (valor) {
              setState(() {
                _botonSecundario = valor;
                prefs.colorSecundario = valor;
              });
            },
          ),
          Text('    Horario de entrega'),
          RadioListTile(
            value: 1,
            title: Text('1 PM'),
            groupValue: _horario,
            onChanged: _setSelectedHorario,
          ),
          RadioListTile(
            value: 2,
            title: Text('2 PM'),
            groupValue: _horario,
            onChanged: _setSelectedHorario,
          ),
          RadioListTile(
            value: 3,
            title: Text('3 PM'),
            groupValue: _horario,
            onChanged: _setSelectedHorario,
          ),

          // RadioListTile(
          //   value: 1,
          //   title: Text('Masculino'),
          //   groupValue: _genero,
          //   onChanged: _setSelectedRadio,
          // ),
          // RadioListTile(
          //   value: 2,
          //   title: Text('Femenino'),
          //   groupValue: _genero,
          //   onChanged: _setSelectedRadio,
          // ),

          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                  labelText: 'Nombre', helperText: 'Nombre de la persona'),
              onChanged: (value) {
                setState(() {
                  _nombre = value;
                  prefs.nombre = value;
                });
              },
            ),
          ),
          _crearDrtopDown()
        ],
      ),
      drawer: MenuWidget(),
    );
  }

  Widget _crearDrtopDown() {
    return Row(
      children: <Widget>[
        Icon(Icons.select_all),
        SizedBox(
          width: 30.0,
        ),
        Expanded(
          child: DropdownButton(
          value: _ubicacion,
          items: getOpciones(),
          onChanged: _setSelectedUbicacion,
        ),
        )
      ],
    );
  }

  List<DropdownMenuItem<String>> getOpciones() {
    List<DropdownMenuItem<String>> lista = new List();
    _ubicaciones.forEach((ubicacion) {
      lista.add(DropdownMenuItem(
        child: Text(ubicacion),
        value: ubicacion,
      ));
    });
    return lista;
  }
}

Widget _Appbar(BuildContext context, String titulo,PreferenciasUsuario prefs) {
  return AppBar(
    title: Text(titulo),
    backgroundColor: (prefs.colorSecundario)
        ? Colors.orange
        : Theme.of(context).primaryColor,
  );

}