import 'package:flutter/material.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';
import 'package:mi_fondita/src/utils/hex_colors_util.dart';
import 'package:mi_fondita/src/widgets/menu_widget.dart';

class InicioPage extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  static final String routeName = 'inicio';

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    prefs.ultimaPagina = InicioPage.routeName;
    return Scaffold(
      appBar: _Appbar(context, 'Mi Fondita', prefs),
      body: Stack(
        children: <Widget>[
          // Container(
          //   width: _screenSize.width,
          //   height: _screenSize.height,
          //   color: Colors.red,
          //   child: Image(
          //     image: AssetImage('assets/images/fondo.jpg'),
          //     fit: BoxFit.cover,
          //   ),
          // ),
          // Container(
          //   width: _screenSize.width,
          //   height: _screenSize.height,
          //   color: Color.fromRGBO(255, 255, 255, 0.6),
          // ),
          SingleChildScrollView(
              child: Column(
            children: <Widget>[
              _bienvenida(prefs),
              foodCarousel(context),
              _titulos('Sopas (1er Tiempo)', '250 ml', Colors.green),
              sopas(context),
              _titulos('Secos (2do Tiempo)', '250 grs', Colors.green),
              secos(context),
              _titulos('Bebidas', '450 ml', Colors.green),
              _bebidas(context),
              _titulos('Postre', '100 grs', Colors.green),
              _postres(context),
              _enviarPedido(),
              _footer(prefs)
            ],
          ))
        ],
      ),
      drawer: MenuWidget(),
    );
  }
}

Widget foodCarousel(BuildContext context) {
  List imagenes = [
    'comidai.jpg',
    'comida2.jpg',
    'comida3.jpg',
    'comida4.jpg',
    'comida5.jpg'
  ];
  final _screenSize = MediaQuery.of(context).size;
  final _pageController =
      new PageController(initialPage: 1, viewportFraction: 1);
  return Container(
    height: _screenSize.height * 0.4,
    width: _screenSize.width,
    child: PageView.builder(
      pageSnapping: true,
      controller: _pageController,
      //children: _tarjetas(context),
      itemCount: imagenes.length,
      itemBuilder: (context, i) {
        //return _tarjeta(context, imagenes[i]);
        return menu(context, imagenes[i]);
      },
    ),
  );
}

Widget menu(BuildContext context, String imagen) {
  return Stack(
    children: <Widget>[
      Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.blue,
        child: Image(
          image: AssetImage('assets/images/$imagen'),
          fit: BoxFit.cover,
        ),
        margin: EdgeInsets.symmetric(horizontal: 2.0, vertical: 5.0),
      ),
      // Text('Platillo')
    ],
  );
}

Widget _tarjeta(BuildContext context, String imagen) {
  final tarjeta = Container(
    width: 500.0,
    height: 400.0,
    margin: EdgeInsets.only(right: 1.0),
    child: Column(
      children: <Widget>[
        Container(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: FadeInImage(
              image: AssetImage('assets/images/$imagen'),
              placeholder: AssetImage('assets/images/no-image.jpg'),
              fit: BoxFit.cover,
              height: 90.0,
            ),
          ),
        ),
        SizedBox(height: 5.0),
        Text(
          'Platillo',
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.caption,
        )
      ],
    ),
  );
  return GestureDetector(
    onTap: () {
      //Navigator.pushNamed(context, 'detalle', arguments: pelicula);
    },
    child: tarjeta,
  );
}

Widget sopas(BuildContext context) {
  final _screenSize = MediaQuery.of(context).size;
  return Container(
    width: double.infinity,
    // color: Colors.green,
    // height: _screenSize.height * 0.15,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        itemMenu('Sopa aguada', 'sopafideo.jpg'),
        itemMenu('Sopa de verdura', 'sopaverduras.jpg'),
        itemMenu('Lentejas', 'sopalentejas.jpg'),
      ],
    ),
  );
}

Widget secos(BuildContext context) {
  final _screenSize = MediaQuery.of(context).size;
  return Container(
    width: double.infinity,
    // color: Colors.green,
    // height: _screenSize.height * 0.15,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        itemMenu('Arroz blanco', 'arroz.jpg'),
        itemMenu('Sopa de codito', 'coditos.jpg'),
      ],
    ),
  );
}

Widget _bebidas(BuildContext context) {
  final _screenSize = MediaQuery.of(context).size;
  return Container(
    width: double.infinity,
    // color: Colors.green,
    // height: _screenSize.height * 0.15,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        itemMenu('Frutas de temporada', 'aguasandia.jpg'),
        itemMenu('Limon', 'agua1.jpg'),
        itemMenu('Piña', 'aguapina.jpg'),
      ],
    ),
  );
}

Widget _postres(BuildContext context) {
  final _screenSize = MediaQuery.of(context).size;
  return Container(
    width: double.infinity,
    // color: Colors.green,
    // height: _screenSize.height * 0.15,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        itemMenu('Arroz con leche', 'arrozleche.jpg'),
      ],
    ),
  );
}

Widget itemMenu(String titulo, String imagen) {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          height: 90.0,
          width: 90.0,
          child: ClipRRect(
            child: Image(
              image: AssetImage('assets/images/$imagen'),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(50.0),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(90.0),
            color: HexColor('fcc613'),
          ),
        ),
        Container(
          width: 100.0,
          child: Center(
              child: Text(
            titulo,
            overflow: TextOverflow.ellipsis,
            textScaleFactor: 0.9,
          )),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: HexColor('fcc613'),
          ),
        )
      ],
    ),
  );
}

Widget _titulos(String titulo, String porcion, Color color) {
  return Container(
    width: double.infinity,
    margin: EdgeInsets.all(5.0),
    padding: EdgeInsets.only(left: 5.0),
    child: Row(
      children: <Widget>[
        Text(
          titulo,
          style: TextStyle(fontSize: 25.0),
        ),
        Text(
          porcion,
          style: TextStyle(fontSize: 15.0, color: Colors.blueGrey),
        ),
      ],
    ),
    decoration: BoxDecoration(
      color: Colors.green,//HexColor('ccce00'),
      borderRadius: BorderRadius.circular(20.0),
      // border:new Border.all(color:HexColor('a8ff3e'),width: 2.0)
    ),
  );
}

Widget _enviarPedido() {
  return FlatButton(
    child: Container(
      child: Center(
          child: Text(
        'Realizar pedido',
        style: TextStyle(color: Colors.white, fontSize: 20.0),
      )),
      width: double.infinity,
      height: 50.0,
      margin: EdgeInsets.symmetric(vertical: 10.0),
      decoration: BoxDecoration(
          color: Colors.lightBlue, borderRadius: BorderRadius.circular(10.0)),
    ),
    onPressed: () {},
  );
}

Widget _bienvenida(PreferenciasUsuario prefs) {
  return Container(
    child: Text(
      'Hola ${prefs.nombre} el menú de hoy es:',
      style: TextStyle(fontSize: 20.0),
    ),
  );
}

Widget _footer(PreferenciasUsuario prefs) {
  return Container(
    child: Text(
      '*Tu pedido sera entregado en ${prefs.ubicacion}',
      style: TextStyle(
        fontSize: 15.0,
      ),
    ),
  );
}

Widget _Appbar(BuildContext context, String titulo, PreferenciasUsuario prefs) {
  return AppBar(
    title: Text(titulo),
    backgroundColor: (prefs.colorSecundario)
        ? Colors.orange
        : Theme.of(context).primaryColor,
  );
}
