import 'package:flutter/material.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';
import 'package:mi_fondita/src/widgets/menu_widget.dart';


class RecetarioPage extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  static final String routeName = 'Recetario';
  @override
  Widget build(BuildContext context) {
    prefs.ultimaPagina = RecetarioPage.routeName;
    return Scaffold(
      appBar: _Appbar(context, 'Recetario', prefs),
      body:_body(context),
      drawer: MenuWidget(),
    );
  }
}



Widget _body(BuildContext context){
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        _tarjetaReceta()
      ],
    ),
  );
}
Widget _tarjetaReceta(){
  return Container(
    padding: EdgeInsets.all(5.0),
    width: double.infinity,
    child: Card(
      elevation: 3.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ClipRRect(
            child: Image(
              image: AssetImage('assets/images/comida1.jpg'),
              fit: BoxFit.fill,
            ),
            borderRadius: BorderRadius.circular(20.0),
          ),
          Text('Chiles rellenos de queso',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18.0),),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Ingredientes:',style: TextStyle(fontWeight: FontWeight.bold),),
              Text('-Ingrediente 1 100grs'),
              Text('-Ingrediente 2 400 ml'),
              Text('-Ingrediente 3 dos cucharadas'),
              Text('Modo de preparación:',style: TextStyle(fontWeight: FontWeight.bold),),
            ],
          )
        ],
      ),
    ),
  );
}

Widget _Appbar(BuildContext context, String titulo,PreferenciasUsuario prefs) {
  return AppBar(
    title: Text(titulo),
    backgroundColor: (prefs.colorSecundario)
        ? Colors.orange
        : Theme.of(context).primaryColor,
  );
}