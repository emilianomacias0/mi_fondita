import 'package:flutter/material.dart';
import 'package:mi_fondita/src/bloc/categorias_bloc.dart';
import 'package:mi_fondita/src/models/categorias_model.dart';
import 'package:mi_fondita/src/providers/categorias_service.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';
import 'package:mi_fondita/src/widgets/menu_widget.dart';

class RegistrCategorias extends StatefulWidget {
  static final String routeName = 'RegistroCategorias';
  _RegistrCategoriasState createState() => _RegistrCategoriasState();
}

class _RegistrCategoriasState extends State<RegistrCategorias> {
  final categoriasBloc = CategoriasBloc();
  final categoriaProvider = CategoriasProvider();
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final prefs = new PreferenciasUsuario();
  CategoriaModel categoria = new CategoriaModel();
  bool _guardando = false;
  @override
  Widget build(BuildContext context) {
      categoriasBloc.obtenerCategorias();
    return Scaffold(
      appBar: AppBar(
        title: Text('Registrar Categorias'),
        backgroundColor: (prefs.colorSecundario) ? Colors.orange : Colors.blue,
      ),
      body: Column(
        children: <Widget>[
          Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                _crearInput(),
                _crearBoton(),
                SizedBox(
                  height: 400,
                  child: _crearListado(),
                )
              ],
            ),
          )
        ],
      ),
      drawer: MenuWidget(),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      initialValue: categoria.nombre,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Producto',
      ),
      onSaved: (value) => categoria.nombre = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese el nombre de la categoria';
        }
      },
    );
  }

  Widget _crearBoton() {
    return RaisedButton.icon(
      icon: Icon(Icons.save),
      onPressed: _guardando ? null : _submit,
      label: Text('Guardar'),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      color: Colors.deepPurple,
      textColor: Colors.white,
    );
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;
    formKey.currentState.save();
    Map<String, dynamic> data;
    setState(() {
      _guardando = true;
    });
    // if (producto.id != null) {
    //   categoriaProvider.editarProducto(producto);
    // } else {
    data = await categoriaProvider.crearCategoria(categoria);
    print('Data de post');
    print(data['ok']);
    if (data['ok']) {
      mostrarSnakBar(data['mensaje']);
      Navigator.pop(context);
    }
    //}
  }

  void mostrarSnakBar(String mensaje) {
    final snakBar = SnackBar(
      content: Text(mensaje),
      duration: Duration(microseconds: 2500),
    );
    scaffoldKey.currentState.showSnackBar(snakBar);
  }

  Widget _crearListado() {
    return FutureBuilder<List<CategoriaModel>>(
      future: categoriaProvider.obtenerCategorias(),
      builder:
          (BuildContext context, AsyncSnapshot<List<CategoriaModel>> snapshot) {
        if (snapshot.hasData) {
          final categorias = snapshot.data;
          return ListView.builder(
            itemCount: categorias.length,
            itemBuilder: (context, i) => _crearItem(categorias[i], context),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }



   Widget _crearItem(CategoriaModel categoria, BuildContext context) {
    return Dismissible(
      key: UniqueKey(),
      child:ListTile(
      title: Text(categoria.nombre),
      subtitle: Text(categoria.fecha.toUtc().toString()),
      leading: Icon(Icons.category),
      trailing: Icon(
        Icons.arrow_right,
        color: Colors.blue,
      ),
    ) ,
    onDismissed: (direccion)=>categoriasBloc.borrarCategoria(categoria.id) ,
    );
  }

  Widget _crearInput() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        //autofocus: true,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
            hintText: 'Categoria',
            labelText: 'Categoria',
            helperText: 'Categoria',
            suffixIcon: Icon(Icons.category),
            icon: Icon(Icons.check_circle)),
        initialValue: categoria.nombre,
        onSaved: (value) => categoria.nombre = value,
        validator: (value) {
          if (value.length < 3) {
            return 'Ingrese el nombre de la categoria';
          }
        },
      ),
    );
  }
}
