import 'package:flutter/material.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';
import 'package:mi_fondita/src/widgets/app_bar_widget.dart';
import 'package:mi_fondita/src/widgets/menu_widget.dart';

class AlaCartaPage extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  static final String routeName = 'alaCarta';
  @override
  Widget build(BuildContext context) {
    prefs.ultimaPagina = AlaCartaPage.routeName;
    return Scaffold(
      appBar: _Appbar(context, 'A la carta', prefs),
      body: _body(),
      drawer: MenuWidget(),
    );
  }
}

Widget _body() {
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[_tarjetaAlaCarta()],
    ),
  );
}

Widget _tarjetaAlaCarta() {
  return Container(
    padding: EdgeInsets.all(5.0),
    width: double.infinity,
    child: Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      elevation: 3.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ClipRRect(
            child: Image(
              image: AssetImage('assets/images/comida3.jpg'),
              fit: BoxFit.fill,
            ),
            borderRadius: BorderRadius.circular(20.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                'Tacos de Carnitas',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0),
              ),
              Text(
                '3 pzs',
                style: TextStyle(fontSize: 15.0, color: Colors.grey),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                      'Deliciosos tacos de carnitas estilo michoacan, acompañados de cilantro,cebolla y salsa a tu elección')),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Text('\$ 45.00 MXN'),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    ),
  );
}

Widget _Appbar(BuildContext context, String titulo,PreferenciasUsuario prefs) {
  return AppBar(
    title: Text(titulo),
    backgroundColor: (prefs.colorSecundario)
        ? Colors.orange
        : Theme.of(context).primaryColor,
  );
}
