import 'dart:async';

import 'package:mi_fondita/src/models/categorias_model.dart';
import 'package:mi_fondita/src/providers/categorias_service.dart';

class CategoriasBloc {
  static final CategoriasBloc _singleton = new CategoriasBloc._internal();
  static final categoriaProvider = new CategoriasProvider();
  factory CategoriasBloc() {
    return _singleton;
  }

  CategoriasBloc._internal() {
    obtenerCategorias();
  }

  final _categoriasController = StreamController<List<CategoriaModel>>.broadcast();

  Stream<List<CategoriaModel>> get categoriaStream => _categoriasController.stream;

  dispose() {
    _categoriasController?.close();
  }

  obtenerCategorias() async {
    _categoriasController.sink.add(await categoriaProvider.obtenerCategorias());
  }

  agregarCategoria(CategoriaModel categoria) async {
    await categoriaProvider.crearCategoria(categoria);
    obtenerCategorias();
  }

  borrarCategoria(String id) async {
    await categoriaProvider.eliminaCategoria(id);
    obtenerCategorias();
  }
}
