import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  //patron sigleton siempre retornara la misma instancia
  static final _instancia = new PreferenciasUsuario._internal();
  factory PreferenciasUsuario() {
    return _instancia;
  }
  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  //Geters y setters

  get genero {
    return _prefs.getInt('genero') ?? 1;
  }

  set genero(int value) {
    _prefs.setInt('genero', value);
  }

//Color secundario
  get colorSecundario {
    return _prefs.getBool('colorSecundario') ?? false;
  }

  set colorSecundario(bool value) {
    _prefs.setBool('colorSecundario', value);
  }

  //Nombre

  get nombre {
    return _prefs.getString('nombre') ?? 'Emiliano';
  }

  set nombre(String value) {
    _prefs.setString('nombre', value);
  }

  //Ultima pagina abierta

  get ultimaPagina {
    return _prefs.getString('ultimaPagina') ?? 'home';
  }

  set ultimaPagina(String valor) {
    _prefs.setString('ultimaPagina', valor);
  }
  //Ninguna de estas se usa
  // bool _colorSecundario;
  // int _genero;
  // String _nombre;

// Horario
  get horario {
    return _prefs.getInt('horario') ?? 1;
  }

  set horario(int value) {
    _prefs.setInt('horario', value);
  }

  //Ubicacion

  get ubicacion {
    return _prefs.getString('ubicacion') ?? 'Tres rios';
  }

  set ubicacion(String value) {
    _prefs.setString('ubicacion', value);
  }
}
