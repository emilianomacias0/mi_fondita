
import 'dart:convert';

CategoriaModel categoriaModelFromJson(String str) => CategoriaModel.fromJson(json.decode(str));

String categoriaModelToJson(CategoriaModel data) => json.encode(data.toJson());

class CategoriaModel {
    String id;
    String nombre;
    bool estado;
    DateTime fecha;

    CategoriaModel({
        this.id,
        this.nombre = '',
        this.estado = true,
        this.fecha,
    });

    factory CategoriaModel.fromJson(Map<String, dynamic> json) => new CategoriaModel(
        id            : json["_id"],
        nombre        : json["nombre"],
        estado        : json["estado"],
        fecha         : DateTime.parse(json["fecha"]),
    );

    Map<String, dynamic> toJson() => {
      //  "_id"            : id,
        "nombre"        : nombre,
       // "estado"        : estado,
        //"fecha"         : fecha,
    };
}
