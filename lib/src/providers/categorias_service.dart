import 'dart:convert';
import 'package:mi_fondita/src/models/categorias_model.dart';
import 'package:http/http.dart' as http;

class CategoriasProvider {
  final String _url = 'http://192.168.1.3:3000';

  Future<List<CategoriaModel>> obtenerCategorias() async {
    final url = '$_url/categorias';
    final response = await http.get(url);
    final productos = new List<CategoriaModel>();
    final Map<String, dynamic> decodedData = json.decode(response.body);
    final data = decodedData['categorias'];
    if (data == null) return null;
    data.forEach((prod) {
      final prodTemp = CategoriaModel.fromJson(prod);
      productos.add(prodTemp);
    });
    return productos;
  }

  Future<Map<String, dynamic>> crearCategoria(CategoriaModel categoria) async {
    final url = '$_url/categorias';
    final response = await http.post(Uri.parse(url),
        body: {"nombre": categoria.nombre},
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    final decodedData = json.decode(response.body);
    print(decodedData);
    return decodedData;
  }

  Future<Map<String, dynamic>> eliminaCategoria(String id) async {
    final url = '$_url/categorias/' + id;
    final response = await http.delete(Uri.parse(url));
    final decodedData = json.decode(response.body);
    print(decodedData);
    return decodedData;
  }
}
