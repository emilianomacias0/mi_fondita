import 'package:flutter/material.dart';
import 'package:mi_fondita/src/pages/alacarta_page.dart';
import 'package:mi_fondita/src/pages/inicio_page.dart';
import 'package:mi_fondita/src/pages/recetario_page.dart';
import 'package:mi_fondita/src/pages/registro_categorias_page.dart';
import 'package:mi_fondita/src/pages/settings_page.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';

class MenuWidget extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Container(
              width: 100.0,
              height: 100.0,
              child: Center(
                child: CircleAvatar(
                  backgroundColor: Colors.red,
                  maxRadius: 70.0,
                  minRadius: 10.0,
                  backgroundImage: AssetImage('assets/images/avatar.jpg'),                 
                ),
              ),
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/menu-img.jpg'),
                  fit: BoxFit.cover),
            ),
          ),
          ListTile(
            leading: Icon(Icons.pages,
                color: (prefs.colorSecundario) ? Colors.orange : Theme.of(context).primaryColor),
            title: Text('Inicio'),
            onTap: () {
              Navigator.pushReplacementNamed(context, InicioPage.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.format_list_bulleted,
                color: (prefs.colorSecundario) ? Colors.orange : Theme.of(context).primaryColor),
            title: Text('A la carta'),
            onTap: () {
              Navigator.pushReplacementNamed(context, AlaCartaPage.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.folder_open,
                color: (prefs.colorSecundario) ? Colors.orange : Theme.of(context).primaryColor),
            title: Text('Recetario'),
            onTap: () {
              Navigator.pushReplacementNamed(context, RecetarioPage.routeName);
            },
          ),
          ListTile(
              leading: Icon(Icons.settings,
                  color: (prefs.colorSecundario) ? Colors.orange : Theme.of(context).primaryColor),
              title: Text('Configuracion'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushReplacementNamed(context, SettingsPage.routeName);
              }),
               ListTile(
              leading: Icon(Icons.settings,
                  color: (prefs.colorSecundario) ? Colors.orange : Colors.blue),
              title: Text('Registro'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushReplacementNamed(context, RegistrCategorias.routeName);
              })
        ],
      ),
    );
  }
}
