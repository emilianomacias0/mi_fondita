

import 'package:flutter/material.dart';
import 'package:mi_fondita/src/sharePrefs/preferencias_usuario.dart';


class AppBarWidget extends AppBar {
  final String titulo;
  final prefs = new PreferenciasUsuario();

 AppBarWidget(this.titulo);

  @override
  Widget build(BuildContext context) {
    return AppBar(
        title: Text(titulo,style: TextStyle(color: Colors.white),),
        backgroundColor: (prefs.colorSecundario) ? Colors.orange : Theme.of(context).primaryColor,
      );
  }
}